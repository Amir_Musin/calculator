package android.itis.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button btn_addition;
    private Button btn_divide;
    private Button btn_result;
    private Button btn_clear;
    private TextView tv_text;
    private String result;
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_addition=(Button)findViewById(R.id.btn_addition);
        btn_divide=(Button)findViewById(R.id.btn_divide);
        btn_result=(Button)findViewById(R.id.btn_result);
        btn_clear=(Button)findViewById(R.id.btn_clear);
        tv_text=(TextView)findViewById(R.id.tv_text);

    }
    public void buttonClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btn_addition:
                if (tv_text.getText().length()>0)
                if (Character.isLetter(tv_text.getText().toString().charAt(0)))
                    tv_text.setText("");
                tv_text.setText(tv_text.getText().toString()+"+");
                break;
            case R.id.btn_divide:
                if (tv_text.getText().length()>0)
                if (Character.isLetter(tv_text.getText().toString().charAt(0)))
                    tv_text.setText("");
                tv_text.setText(tv_text.getText().toString()+"-");
                break;
            case R.id.btn_result:
                if (tv_text.getText().length()>0)
                if (isError(tv_text.getText().toString()))
                    tv_text.setText("Неправильный ввод");
                else {
                    result=String.valueOf(evaluation(tv_text.getText().toString()));
                    tv_text.setText(result);
                }
                break;
            case R.id.btn_clear:
            tv_text.setText("");
                break;
            default:
                if (tv_text.getText().length()>0)
                if (Character.isLetter(tv_text.getText().toString().charAt(0)))
                    tv_text.setText("");
                tv_text.setText(tv_text.getText().toString()+v.getContentDescription().toString());
                break;
        }
    }
    private int eval(int currentNumber, int nextNumber,char c){
        if (c=='+')
            currentNumber=currentNumber+nextNumber;
        else
            currentNumber=currentNumber-nextNumber;
        return currentNumber;
    }
    private int evaluation(String text){
        int currentNumber=0;
        int nextNumber=0;
        int pos=0;
        String s="";
        if (!isOperation(text.charAt(0))){
            while (pos<text.length()&&!isOperation(text.charAt(pos))){
                s+=text.charAt(pos);
                pos++;
            }
            currentNumber=Integer.valueOf(s);
        }
        for (int i=pos;i<text.length();i++){
                int posOfOperation=i;
                i++;
                s="";
                while (i<text.length()&&!isOperation(text.charAt(i))){
                    s+=text.charAt(i);
                    i++;
                }
            i--;
                 nextNumber=Integer.valueOf(s);
                currentNumber= eval(currentNumber,nextNumber,text.charAt(posOfOperation));
        }

        return currentNumber;
    }
    private boolean isOperation(char s){
        switch (s) {
            case '+':
                return true;
            case '-':
                return true;


        }
        return false;
    }
    private boolean isError(String text){
        if (isOperation(text.charAt(text.length()-1)))
            return true;
        for (int i=0;i<text.length()-1;i++){
            if (isOperation(text.charAt(i)) && isOperation(text.charAt(i+1)))
                return true;
        }
        if (isOperation(text.charAt(0))&&(text.length()<2))
            return true;
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
